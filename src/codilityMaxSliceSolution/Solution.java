package codilityMaxSliceSolution;

public class Solution {
    public int solution(int[] A) {
        int tempSum = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < A.length; ++i) {
            tempSum = tempSum + A[i];
            if (tempSum > max) {
                max = tempSum;
            }
            if (tempSum <= 0) {
                tempSum = 0;
            }
        }
        return max;
    }
}
